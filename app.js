const express = require("express");
const cors = require("cors");
require("dotenv").config();
const customerRoute = require("./customer/customer.route");

const app = express();
app.use(cors());
app.listen(3000);
app.use(customerRoute);

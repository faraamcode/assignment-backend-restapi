const Pool = require("pg").Pool;
const user = process.env.DB_USER;
const password = process.env.DB_PASSWORD;
const host = process.env.DB_HOST;
const database = process.env.DB_DATABASE;
const pool = new Pool({
  user,
  password,
  host,
  database,
});

module.exports = pool;
